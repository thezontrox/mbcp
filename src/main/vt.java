/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import java.util.Arrays;

/**
 *
 * @author gmfor
 */
public class vt {
    public int[] VectorTiempo = new int[6]; 
    public int identificador;
    public vt(int identificador){
        VectorTiempo[0] = 0; // Proceso 1
        VectorTiempo[1] = 0; // Porceso 2
        VectorTiempo[2] = 0; // Porceso 3
        VectorTiempo[3] = 0; // Porceso 4
        VectorTiempo[4] = 0; // Porceso 5
        VectorTiempo[5] = 0; // Porceso 6
        this.identificador = identificador;
    }

    public int getVectorTiempo(int posicion) {
        return VectorTiempo[posicion-1];
    }

    public void setVectorTiempo(int posicion, int valor) {
        this.VectorTiempo[posicion-1] = valor;
    }
    
    

    @Override
    public String toString() {
        String tiempo = Arrays.toString(VectorTiempo);
        return tiempo;
    }
    
}
