/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import java.util.Arrays;



/**
 *
 * @author gmfor
 */
public class ci {
    public int[] CI = new int[2];
    public int identificador;
    public ci(int identificador){
        CI[0] = 0; // L
        CI[1] = 0; // TL
        this.identificador = identificador;
        
    }

    public int[] getCI() {
        return CI;
    }

    public void setCI(int[] CI) {
        this.CI = CI;
    }

    @Override
    public String toString() {
        String result = Arrays.toString(CI);
        return result;
    }
    
    
    
}
