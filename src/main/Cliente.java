package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {
    
    int[] puertos = new int[6];
    String enviarMensaje = "";

    public void startClient(int IProcesoEviar, String mensaje, int k, int tk, String hm) {
        puertos[0] = 4000;
        puertos[1] = 5000;
        puertos[2] = 6000;
        puertos[3] = 7000;
        puertos[4] = 8000;
        puertos[5] = 9000;
        Socket s = null;
        System.out.println("Mensaje enviado a proceso: " + IProcesoEviar);
        String host = "localhost";
        
        
        enviarMensaje += k;
        enviarMensaje += ",";
        enviarMensaje += tk;
        enviarMensaje += ",";
        enviarMensaje += mensaje;
        enviarMensaje += ",";
        enviarMensaje += hm;

        try {
            int serverPort = puertos[IProcesoEviar -1];
            s = new Socket(host, serverPort);
            DataInputStream in = new DataInputStream(s.getInputStream());
            DataOutputStream out = new DataOutputStream(s.getOutputStream());
            out.writeUTF(enviarMensaje); // Aqui se envia el mensaje
            String data = in.readUTF();
            System.out.println("Envio: " + data);
        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if (s != null) try {
                s.close();
            } catch (IOException e) {
                System.out.println("Error " + e.getMessage());
            }
        }
    }
}
